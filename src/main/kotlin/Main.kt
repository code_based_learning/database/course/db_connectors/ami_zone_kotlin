package org.example

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

fun main() {
    try {
        // Java-like
        connectToAmiZoneV1(port = 3312, user = "root", password = "root").use { connection ->
            connection.printAllPizzasV1()
        }

        // with "exposed"
        connectToAmiZoneV2(port = 3312, user = "root", password = "root").apply {
            printAllPizzasV2()
        }
    } catch (e: ExposedSQLException) {
        println("Exposed-SQL-Error: $e")
    } catch (e: SQLException) {
        println("SQL-Error: $e")
    } catch (e: Exception) {
        println("Error: $e")
    }
}

fun connectToAmiZoneV1(port: Int, user: String, password: String) =
    // sometimes this is needed: ?useSSL=false&allowPublicKeyRetrieval=true
    DriverManager.getConnection("jdbc:mariadb://localhost:$port/ami_zone", user, password)!!

@Throws(SQLException::class)
fun Connection.printAllPizzasV1() {
    // use a prepare statement
    val sql = "SELECT * FROM shop_product WHERE name like ?"
    prepareStatement(sql).apply {
        setString(1, "%Pizza%")
        // traverse the result set
        executeQuery().apply {
            while (next()) {
                println("[V1] - ${getString("name")}, ${getString("price")}")
            }
        }
    }
}

/*
    Exposed can be considered an Object-Relational Mapping (ORM) library for Kotlin,
    but it does not follow the traditional approach of ORMs like Hibernate or Entity Framework.
    Rather, it provides a Kotlin-style API for SQL operations in both a typesafe SQL DSL
    and a DAO (Data Access Object) style.

    DAO style: you work with classes and objects rather than SQL queries, similar
    to traditional ORMs.
    Because you can still write raw SQL queries and because JetBrains strives to
    expose SQL functionality in a typesafe and Kotlin-friendly way, it can feel
    quite different and sometimes more "low-level" than other, more abstract
    ORMs like Hibernate.
 */

// define tables according to the framework

object Product : IntIdTable("shop_product") {
    val name = varchar("name", 50)
    val price = double("price")
    val categoryId = reference("category_id", Category.id)
}

object Category : IntIdTable("shop_category") {
    val name = varchar("name", 50)
}

// as before
fun connectToAmiZoneV2(port: Int, user: String, password: String) =
    // sometimes this is needed: ?useSSL=false&allowPublicKeyRetrieval=true
    Database.connect("jdbc:mariadb://localhost:$port/ami_zone", user = user, password = password)

fun Database.printAllPizzasV2() {
    transaction {
        // as before, just inside a transaction
        for (user in Product.select { Product.name like "%Pizza%" } ) {
            println("[V2] - ${user[Product.name]}, ${user[Product.price]}")
        }

        // now with join (cf. also the resulting SQL query from the log output)
        val productJoinsCategory = Product.join(Category, JoinType.INNER,
            additionalConstraint = { Product.categoryId eq Category.id })

        for (row in productJoinsCategory.select { Product.name like "%Pizza%" } ) {
            println("[V3] - ${row[Product.name]}, ${row[Product.price]} in ${row[Category.name]}")
        }
    }
}
